#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "Engine/StaticMesh.h"
#include "Engine/SkeletalMesh.h"
#include "MyBlueprintFunctionLibrary.generated.h"

class UAssetUserData;

UCLASS()
class SKOTOS_API UMyBlueprintFunctionLibrary : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()


	/**
	* 対象のオブジェクトがもつAssetUserDataを取得する
	* @param UObject* TargetObject							対象となるオブジェクト
	* @param TSubclassOf<UAssetUserData> AssetUserDataClass 指定のクラス名
	* @param TArray<UAssetUserData*>& OutAssetUserData		取得したAssetUserDataの配列
	*/
	UFUNCTION(BlueprintCallable,Category = "Utilities",meta = (DeterminesOutputType = "AssetUserDataClass",DynamicOutputParam = "OutAssetUserData" ))
	static void GetAssetUserDataOfClass(UStaticMesh* TargetObject,TSubclassOf<UAssetUserData> AssetUserDataClass,TArray<UAssetUserData*>& OutAssetUserData);

};
