#pragma once

#include "CoreMinimal.h"
#include "Engine/AssetUserData.h"
#include "Engine/StaticMesh.h"

#include "MyAssetUserData.generated.h"

/**
 * 
 */
UCLASS(BlueprintType)
class SKOTOS_API UMyAssetUserData : public UAssetUserData
{
	GENERATED_BODY()

public:
	UPROPERTY(BlueprintReadOnly, EditAnywhere)
		FString data;

};