#include "MyBlueprintFunctionLibrary.h"


void UMyBlueprintFunctionLibrary::GetAssetUserDataOfClass(UStaticMesh* TargetObject,TSubclassOf<UAssetUserData> AssetUserDataClass,TArray<UAssetUserData*>& OutAssetUserData)
{
	OutAssetUserData.Reset();

	IInterface_AssetUserData* Interface = Cast<IInterface_AssetUserData>(TargetObject);
	if( Interface == nullptr )
	{
		return;
	}

	auto Array = (*Interface->GetAssetUserDataArray());

	for (auto Data : Array )
	{
		if( Data != NULL && Data->IsA(AssetUserDataClass) )
		{
			OutAssetUserData.Add(Data);
		}
	}
}

//void UMyBlueprintFunctionLibrary::GetAssetUserDataOfClass(USkeletalMesh* TargetObject, TSubclassOf<UAssetUserData> AssetUserDataClass, TArray<UAssetUserData*>& OutAssetUserData)
//{
//	OutAssetUserData.Reset();
//
//	IInterface_AssetUserData* Interface = Cast<IInterface_AssetUserData>(TargetObject);
//	if (Interface == nullptr)
//	{
//		return;
//	}
//
//	auto Array = (*Interface->GetAssetUserDataArray());
//
//	for (auto Data : Array)
//	{
//		if (Data != NULL && Data->IsA(AssetUserDataClass))
//		{
//			OutAssetUserData.Add(Data);
//		}
//	}
//}

